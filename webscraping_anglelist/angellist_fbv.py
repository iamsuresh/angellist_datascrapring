# web scraping for timejobs
# try to giv multiple inputs and condition should accept
from bs4 import BeautifulSoup
import requests
import time   # run prgm forever with specified time


####################
print('put some skill that you are familiar with')
prefferd_skill=input('>')
print(f'Filtering out {prefferd_skill}')
##################

def find_jobs():
    #html_text is also can write instead of source ......it's just our own name
    source=requests.get('https://angel.co/jobs').text
    soup=BeautifulSoup(source, 'lxml')

    # jobs=soup.find_all('li',class_='joblist-comp-name')   used to find all the jobs(find.all)
    jobs=soup.find_all('li',class_='clearfix job-bx wht-shd-bx')
    for index, job in enumerate(jobs):
        published_date=job.find('span',class_='styles_posted__2iAUR styles_recentlyPosted__3hWcP styles_tablet__2JANA').span.text
        if 'WEEK' in published_date:   #condition for only jobs posted few days ago
            company_name=job.find('h2',class_='styles_name__v6k3s').text #.replace(' ','')  #.text is used to find the perticular text and replace is used to delete the unwanted space
            skills=job.find('span',class_='srp-skills').text.replace(' ','')
            more_info=job.header.h2.a['href']

            #########
            if prefferd_skill in skills:
            #########
                with open(f'post/{index}.txt','w') as f:
                # print('company Name: ',company_name) 
                    f.write(f'Company Name: {company_name.strip()}\n') #f is known as FORMATED STRING f is used when '' cots are used 
                    f.write(f'Required Skills: {skills.strip()}\n') # .strip() is used for proper allignment and also print in same line
                    f.write(f'More Info: {more_info}')
                print(f'File saved: {index}')

            print('-'*50)

if __name__=='__main__':
    while True:
        find_jobs()
        time_wait=30
        # print(f'waiting {time_wait} seconds....')
        print(f'waiting {time_wait} minutes....')
        time.sleep(time_wait*60)

