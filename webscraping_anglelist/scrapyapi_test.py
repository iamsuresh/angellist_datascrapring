# remember to install the library: pip install scraperapi-sdk
from scraper_api import ScraperAPIClient
from bs4 import BeautifulSoup
import requests
import time

headers={'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36'}

client = ScraperAPIClient('55b7f5b32dea95250b27edce13221ced')
result = client.get(url = 'https://angel.co/jobs', headers=headers).text
print(result);
# Scrapy users can simply replace the urls in their start_urls and parse function
soup=BeautifulSoup(result,features='lxml')

# Note for Scrapy, you should not use DOWNLOAD_DELAY and
# RANDOMIZE_DOWNLOAD_DELAY, these will lower your concurrency and are not
# needed with our API
# ...other scrapy setup code
start_urls =[client.scrapyGet(url = 'https://angel.co/jobs', headers=headers)]
def parse(self, response):
    job=soup.find('div',{'class':'styles_component__11xQN'},headers=headers)
    company_name=job.find('h2',{'class':'styles_name__v6k3s'},headers=headers).text
    print(company_name)

# ...your parsing logic here
    yield client.Request(client.scrapyGet(url = 'https://angel.co/jobs', headers=headers), self.parse)
