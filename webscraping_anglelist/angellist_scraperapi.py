#reverseengineering by free codecamp: youtube 27:45 time
# remember to install the library: pip install scraperapi-sdk
from scraper_api import ScraperAPIClient 
from bs4 import BeautifulSoup
import requests
import time

headers={'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36'}

html_text=requests.get('http://api.scraperapi.com/?api_key=55b7f5b32dea95250b27edce13221ced&url=https://angel.co/jobs',headers=headers)
soup=BeautifulSoup(html_text,features='lxml')

job=soup.find('div',{'class':'styles_component__11xQN'},headers=headers)
company_name=job.find('h2',{'class':'styles_name__v6k3s'},headers=headers).text
print(company_name)
