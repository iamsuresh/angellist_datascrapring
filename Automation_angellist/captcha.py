from selenium import webdriver
from python3_anticaptcha import ImageToTextTask #CallbackClient
from time import sleep
import requests



def GetImageCookies():
    print('Extracting Browser Cookies')
    image_cookies = ''
    for cookie in browser.get_cookies():
        if cookie['name'] == 'ssc':
            image_cookies += 'ssc={};'.format(cookie['value'])
        elif cookie['name'] == 'ghsdfkjlksssalk35bbr':
            image_cookies += 'ghsdfkjlksssalk35bbr={};'.format(cookie['value'])
    # print(image_cookies)
    return image_cookies

def SaveImage(captcha_file = "master.jpg"):
    print('Saving the captcha image')
    header = {
    'Accept': 'image/webp,image/apng,image/*,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en,en-US;q=0.9,ar;q=0.8',
    'Cookie': GetImageCookies(),
    'Host': 'masked',
    'Referer': 'masked',
    'Sec-Fetch-Mode': 'no-cors',
    'Sec-Fetch-Site': 'same-origin',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0'}


    pic = requests.get('https://angel.co/jobs',verify=False,headers = header)
    if pic.status_code == 200:
        with open(captcha_file, 'wb') as f:
            f.write(pic.content)

def SolveCapcha(captcha_file = "master.jpg"):
    print('Solving the captcha image')
    ANTICAPTCHA_KEY = 'masked'  
    result = ImageToTextTask.ImageToTextTask(
        anticaptcha_key=ANTICAPTCHA_KEY).captcha_handler(captcha_file=captcha_file)
    captcha_text = result['solution']['text']
    print('Captcha text is :',captcha_text)
    return captcha_text


browser = webdriver.Chrome()
url = 'https://angel.co/login'
browser.get(url)
def Login():
    SaveImage()
    sleep(30)
    username = browser.find_element_by_id("user_email")
    username.clear()
    username.send_keys(" ")
    password = browser.find_element_by_id("user_password")
    password.clear()
    password.send_keys(" ")
    captcha = browser.find_element_by_id("masked")
    captcha.clear()
    captcha_text = SolveCapcha()
    captcha.send_keys(captcha_text)
    login = browser.find_element_by_id("masked").click()
    sleep(5)
    err_message = browser.find_elements_by_id('masked')
    if err_message :
        if err_message[0].text == 'The verification code is incorrect.':
            print(err_message[0].text)
            return False
    return True


"""The logic here is that the image gets downloaded using the cookies but sometimes
the letters are hard to be solved so each time we download the same image with the
same cookies the content of the image will be the same but how it's written is different
So we keep trying till we get it right """
while Login() == False:
    Login()